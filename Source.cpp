#include <vector>
#include <ctime>
#include <omp.h>
#include <iostream>

typedef long long lli;
typedef long double ld;

const lli NUM_THREADS = 4;
const lli VEC_SIZE    = 10000000;
const ld  DELTA       = 0.000001;
const ld  A           = -2;
const ld  B           = 2;

// scalar
lli scalarLinear(std::vector<lli> vec1, std::vector<lli> vec2) {
    if (vec1.size() != vec2.size()) {
        throw 1;
    }

    lli res = 0;

    for (lli i = 0; i < vec1.size(); i++) {
        res += vec1.at(i) * vec2.at(i);
    }
    return res;
}

lli scalarParallel(std::vector<lli> vec1, std::vector<lli> vec2) {
    if (vec1.size() != vec2.size()) {
        throw 1;
    }

    lli res = 0;

    #pragma omp parallel for reduction(+: res)
    for (lli i = 0; i < vec1.size(); i++) {
        res += vec1.at(i) * vec2.at(i);
    }
    return res;
}

// trapezoidalItegrate
ld trapezoidalItegrateLinear(ld a, ld b, ld delta, ld (*f)(ld)) {
    if (b < a || delta > b - a) {
        throw 1;
    }

    lli N = ceil((b - a) / delta);
    ld res = .0; 

    ld y_pre = (*f)(a + 0 * delta);
    ld y_act;

    for (int i = 1; i <= N; i++) {
        if (i < N) {
            y_act = (*f)(a + i * delta);
        }
        else if (omp_get_thread_num() == omp_get_num_threads()) {
            y_act = (*f)(b);
        }
        res += (y_act + y_pre) * delta / 2;
        y_pre = y_act;
    }

    return res;
}

ld trapezoidalItegrateParallel(ld a, ld b, ld delta, ld(*f)(ld)) {
    if (b < a || delta > b - a) {
        throw 1;
    }

    lli numThreads = NUM_THREADS;

    lli N = ceil((b - a) / delta);
    ld res = .0;
    
    lli opsPerThread = N / numThreads;
    lli restNum = N - opsPerThread * numThreads;

    #pragma omp parallel reduction(+:res)
    {
        lli threadNum = omp_get_thread_num();
        lli threadStart;
        lli threadEnd;

        if (numThreads - threadNum  <= restNum) {
            threadStart = threadNum * opsPerThread +
                opsPerThread + restNum - (numThreads - threadNum);
            threadEnd = threadStart + opsPerThread + 1;
        } else {
            threadStart = threadNum * opsPerThread;
            threadEnd = threadStart + opsPerThread;
        }

        ld y_pre = (*f)(a + threadStart * delta);
        ld y_act;

        for (int i = threadStart + 1; i <= threadEnd; i++) {
            if (i < N || (numThreads - threadNum + 1) == 0) {
                y_act = (*f)(a + i * delta);
            }
            else {
                y_act = (*f)(b);
            }
            ld s = (y_act + y_pre) * delta / 2;
            res += s;
            y_pre = y_act;
        }
    }
    return res;
}

ld exampleFunc(ld x) {
    return x*x;
}


int main() {
    omp_set_num_threads(NUM_THREADS);

    
    std::vector<lli> vec1(VEC_SIZE);
    std::vector<lli> vec2(VEC_SIZE);

    srand(time(NULL));

    #pragma omp parallel for
    for (int i = 0; i < vec1.size(); i++) {
        vec1[i] = rand() % 10;
        vec2[i] = rand() % 10;
    }
     
    double timeLinear = omp_get_wtime();
    lli resLinear = scalarLinear(vec1, vec2);
    timeLinear = omp_get_wtime() - timeLinear;
    printf("SCALAR (L): res = %i time = %g\n", resLinear, timeLinear);

    double timeParallel = omp_get_wtime();
    lli resParallel = scalarParallel(vec1, vec2);
    timeParallel = omp_get_wtime() - timeParallel;
    printf("SCALAR (P): res = %i time = %g\n", resParallel, timeParallel);
    
    timeLinear = omp_get_wtime();
    ld resIntegralLinear = trapezoidalItegrateLinear(A, B, DELTA, exampleFunc);
    timeLinear = omp_get_wtime() - timeLinear;
    printf("\n\nINTEGRAL (L): res = %g time = %g", resIntegralLinear, timeLinear);

    timeParallel = omp_get_wtime();
    ld resIntegralParallel = trapezoidalItegrateParallel(A, B, DELTA, exampleFunc);
    timeParallel = omp_get_wtime() - timeParallel;
    printf("\nINTEGRAL (P): res = %g time = %g\n\n", resIntegralParallel, timeParallel);

    system("pause");

    return 0;
}

/*
#pragma omp single
{
std::cout << "\n";
}

#pragma omp critical
{
std::cout << "\n[" << threadNum << "] : " << y_pre << "-" << y_act;
}
#pragma omp barrier
*/